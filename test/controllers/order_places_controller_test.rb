require 'test_helper'

class OrderPlacesControllerTest < ActionController::TestCase
  setup do
    @order_place = order_places(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:order_places)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create order_place" do
    assert_difference('OrderPlace.count') do
      post :create, order_place: { order_id: @order_place.order_id, place_id: @order_place.place_id }
    end

    assert_redirected_to order_place_path(assigns(:order_place))
  end

  test "should show order_place" do
    get :show, id: @order_place
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order_place
    assert_response :success
  end

  test "should update order_place" do
    patch :update, id: @order_place, order_place: { order_id: @order_place.order_id, place_id: @order_place.place_id }
    assert_redirected_to order_place_path(assigns(:order_place))
  end

  test "should destroy order_place" do
    assert_difference('OrderPlace.count', -1) do
      delete :destroy, id: @order_place
    end

    assert_redirected_to order_places_path
  end
end
