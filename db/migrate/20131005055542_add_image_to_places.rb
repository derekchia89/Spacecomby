class AddImageToPlaces < ActiveRecord::Migration
  def self.up
  	add_column :places, :image_file_name, :string
  	add_column :places, :image_content_type, :binary
  	add_column :places, :image_file_size, :integer
  	add_column :places, :image_updated_at, :datetime

    add_column :users, :photo_file_name, :string
    add_column :users, :photo_content_type, :binary
    add_column :users, :photo_file_size, :integer
    add_column :users, :photo_updated_at, :datetime
  end

  def self.down
  	remove_column :places, :image_file_name
  	remove_column :places, :image_content_type
  	remove_column :places, :image_file_size
  	remove_column :places, :image_updated_at

    remove_column :users, :photo_file_name, :string
    remove_column :users, :photo_content_type, :binary
    remove_column :users, :photo_file_size, :integer
    remove_column :users, :photo_updated_at, :datetime

  end
end

