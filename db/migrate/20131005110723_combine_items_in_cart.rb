class CombineItemsInCart < ActiveRecord::Migration

  def self.up
    # replace multiple items for a single product in a cart with a single item
    Cart.all.each do |cart|
      # count the number of each product in the cart
      sums = cart.line_items.group(:place_id).sum(:quantity)

      sums.each do |place_id, quantity|
        if quantity > 1
          # remove individual items
          cart.line_items.where(:place_id => place_id).delete_all

          # replace with a single item
          cart.line_items.create(:place_id=>place_id, :quantity=>quantity)
        end
      end
    end
  end

  def self.down
    # split items with quantity>1 into multiple items
    LineItem.where("quantity>1").each do |line_item|
      # add individual items
      line_item.quantity.times do 
        LineItem.create :cart_id=>line_item.cart_id,
          :place_id=>line_item.place_id, :quantity=>1
      end

      # remove original item
      line_item.destroy
    end
  end
end

