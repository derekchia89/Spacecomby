class AddDateToOrderPlaces < ActiveRecord::Migration
  def change
    add_column :order_places, :start_date, :date
    add_column :order_places, :end_date, :date
    add_column :order_places, :amount, :float
  end
end
