class AddTwilioToUser < ActiveRecord::Migration
  change_table :users do |t|
	  t.string    :formatted_phone_number
	  t.boolean   :sms_blocked, :default => false, :null => false
	  t.string    :sms_confirmation_code
	  t.datetime  :sms_confirmation_attempted
	  t.string    :sms_confirmed_phone_number
  end
end
