class AddColumnsToPlaces < ActiveRecord::Migration
  def change
  	add_column :places, :start_date, :date
  	add_column :places, :end_date, :date
  	add_column :places, :select_start, :date
  	add_column :places, :select_end, :date
  end
end
