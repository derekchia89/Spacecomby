class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :title
      t.text :description,:limit=>4294967295
      t.integer :maxOccupancy
      t.string :wsType
      t.integer :telNumber
      t.time :startTime
      t.time :endTime
      t.decimal :price
      t.decimal :rating
      t.string :address
      t.integer :postal
      t.string :country
      
      t.timestamps
    end
  end
end
