class CreateOrderPlaces < ActiveRecord::Migration
  def change
    create_table :order_places do |t|
      t.integer :order_id
      t.integer :place_id

      t.timestamps
    end
  end
end
