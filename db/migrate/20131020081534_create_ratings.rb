class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.string :user_id
      t.string :place_id
      t.integer :value, :default => 0

      t.timestamps
    end
  end
end
