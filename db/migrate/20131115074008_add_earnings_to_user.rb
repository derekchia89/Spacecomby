class AddEarningsToUser < ActiveRecord::Migration
  def change
    add_column :users, :earning, :integer, :default => 0, :scale => 2
    add_column :users, :spending, :integer, :default => 0, :scale => 2
  end
end
