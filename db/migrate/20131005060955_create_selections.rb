class CreateSelections < ActiveRecord::Migration
  def change
    create_table :selections do |t|
      t.integer :place_id
      t.integer :facility_id
      t.integer :position
      t.datetime :created_at

      t.timestamps
    end
  end
end
