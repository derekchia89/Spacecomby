# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
['registered', 'banned', 'admin'].each do |role|
  Role.find_or_create_by_name role
end 

puts 'SETTING UP DEFAULT USER LOGIN'
user1 = User.create! first_name: 'admin', last_name: 'user', email: 'info@spacecomby.com', password: 'spacecomby1', password_confirmation: 'spacecomby1', gender: 'male', phone_number: '90030534', address: 'NUS', :confirmed_at => DateTime.now

puts 'ADDING SPECIAL ROLES TO USERS'
user1.add_role :admin
user1.save!
