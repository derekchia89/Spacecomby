var ST = {"disable_header_floating":"y"};


// Call isotope when all images are loaded
// function run_isotope(){
//     "use strict";
//     jQuery(".cpt-items").isotope();
// }


jQuery(document).ready(function(){
    "use strict";
    
    var top_bar= jQuery('#header .top-bar-wrapper').eq(0);
    // var p_top = top_bar.position().top;
    // var top_h = p_top + top_bar.height();
    var fixed_e = jQuery('#header .header-outer-wrapper').eq(0);
    fixed_e.attr('logo-height', jQuery('.logo-wrapper img',fixed_e).height());
    
    var is_changed_e_s = false;
    var down_pd = 24; // 10px;
    var logo_rs_h = 30; 
    
     function setTopPos(){
        if(ST.disable_header_floating==='y'){
            return false ;
        }
        
     if(jQuery(window).width()< 959){
        return ;
     }
         
        var  win_top = jQuery(window).scrollTop(); 
        
        if(win_top > top_h)
                {   
                    fixed_e.addClass('fixed');
               
                    if(is_changed_e_s=== false){
                        is_changed_e_s = true;
                        
                         jQuery('.logo-wrapper',fixed_e).animate({
                                paddingTop: '-='+(down_pd-4)+'px'
                         },200);
                          
                         jQuery('.logo-wrapper img',fixed_e).animate({
                                height: (logo_rs_h)+'px'
                         },200);
                         
                         jQuery('.primary-nav > ul >li >a',fixed_e).animate({
                                paddingTop: '-='+down_pd+'px',
                                paddingBottom: '-='+down_pd+'px'    
                         },200);
                         jQuery('.btn',fixed_e).animate({
                                marginTop: '-='+down_pd+'px'
                         },200);
                         
                          jQuery('.primary-nav > ul >li > ul',fixed_e).animate({
                                top: '-='+(down_pd*2)+'px'
                         },200);
                         
                    }
                    
                }else{
                    
                    if(is_changed_e_s===true){
                        
                        jQuery('.logo-wrapper img',fixed_e).animate({
                                height: (fixed_e.attr('logo-height'))
                         },200);
                         
                        jQuery('.logo-wrapper',fixed_e).animate({
                                paddingTop: '+='+(down_pd-4)+'px'
                         },200);
                         
                         jQuery('.primary-nav > ul >li >a',fixed_e).animate({
                                paddingTop: '+='+down_pd+'px',
                                paddingBottom: '+='+down_pd+'px'    
                         },{ queue: false, duration: 200 });
                         jQuery('.btn',fixed_e).animate({
                                marginTop: '+='+down_pd+'px'
                         },200);
                         
                         jQuery('.primary-nav > ul >li > ul',fixed_e).animate({
                                top: '+='+(down_pd*2)+'px'
                         },200);
                         
                         is_changed_e_s =  false;
                    }
                    
                     fixed_e.removeClass('fixed');
                     fixed_e.css({'top' : ''});
                }
     }
     
    
    jQuery(window).scroll(function () {
        setTopPos();
    });
    
    

    //Select Box
    jQuery('.select-box select').fadeTo(0, 0);
    jQuery('.select-box').each(function() {
        if(jQuery(this).find('option:selected').length) {
            jQuery(this).find('span').text(jQuery(this).find('option:selected').text());
        }
    });
    jQuery('.select-box select').change(function() {
        jQuery(this).parent().find('span').text(jQuery(this).find('option:selected').text());
    });

    //ddsmoothmenu for top-bar navigation
    ddsmoothmenu.init({
        mainmenuid: "top-nav-id", //menu DIV id
        orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'top-nav slideMenu', //class added to menu's outer DIV
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    });

    //ddsmoothmenu for primary navigation
    ddsmoothmenu.init({
        mainmenuid: "primary-nav-id", //menu DIV id
        orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'primary-nav slideMenu', //class added to menu's outer DIV
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
});

    // Primary Navigation for mobile.
    var primary_nav_mobile_button = jQuery('#primary-nav-mobile');
    var primary_nav_cloned;
    var primary_nav = jQuery('#primary-nav-id > ul');

    primary_nav.clone().attr('id','primary-nav-mobile-id').removeClass().appendTo( primary_nav_mobile_button );
    primary_nav_cloned = primary_nav_mobile_button.find('> ul');
        jQuery('#primary-nav-mobile-a').click(function(){
            if(jQuery(this).hasClass('primary-nav-close')){
                jQuery(this).removeClass('primary-nav-close').addClass('primary-nav-opened');
                primary_nav_cloned.slideDown( 400 );
            } else {
                jQuery(this).removeClass('primary-nav-opened').addClass('primary-nav-close');
                primary_nav_cloned.slideUp( 400 );
            }
            return false;
        });
        primary_nav_mobile_button.find('a').click(function(event){
            event.stopPropagation();
        });


    // Thumbnail Hover
    jQuery(".thumb-wrapper").hover(function(){
        jQuery(this).find(".thumb-control-wrapper").animate({ opacity: 1 }, 500).addClass('hover');
    }, function(){
        jQuery(this).find(".thumb-control-wrapper").animate({ opacity: 0 }, 500).removeClass('hover');
    });

    // Hash Change
    // jQuery(window).bind("hashchange", function(){
    //     var url = jQuery.url(); // parse the current page URL
    //     var hashOptions = url.fparam('filter');
       
    //     if(typeof(hashOptions)!=="undefined")
    //     {
    //         jQuery(".cpt-filters a").removeClass("selected");
    //         if(jQuery(".cpt-filters a[href='#filter="+hashOptions+"']").length){
    //              jQuery(".cpt-filters a[href='#filter="+hashOptions+"']").addClass("selected");
    //         }
    //         else{
    //             jQuery(".cpt-filters li:first a").addClass("selected");
    //         }

    //         jQuery(".cpt-items").isotope({filter: hashOptions});
    //     }

    // }).trigger("hashchange");

    // //isotope
    // jQuery(".cpt-items").imagesLoaded(run_isotope);
    
    // // Page Load Event
    // jQuery('.thumb-control-wrapper').each(function(i,el){
    //      var new_overlay_w = jQuery(el).prev('img:first').width() - 20;
    //      var new_overlay_h = jQuery(el).prev('img:first').height() - 20;
    //      jQuery(el).css({'width':new_overlay_w,'height':new_overlay_h});
    //  });
    
    // Browser Resize Event

    jQuery(window).resize(function() {
        jQuery('.thumb-control-wrapper').each(function(i,el){
            
             var new_overlay_w = jQuery(el).prev('img:first').width() - 20;
             var new_overlay_h = jQuery(el).prev('img:first').height() - 20;
             
             jQuery(el).css({'width':new_overlay_w,'height':new_overlay_h});
         });
        
        //isotope
        //jQuery(".cpt-items").imagesLoaded(run_isotope); /// don't need 
        // run_isotope();
    });
    


}); // END Document Ready //
