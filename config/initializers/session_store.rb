# Be sure to restart your server when you modify this file.

SpaceComby::Application.config.session_store :cookie_store, key: '_spacecomby_session'

SpaceComby::Application.config.session_store :active_record_store
