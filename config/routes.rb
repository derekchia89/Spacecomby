SpaceComby::Application.routes.draw do

  resources :reviews

  get "land_page/landingpage"
  resources :orders do
  get 'express', :on => :collection
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", registrations: 'users/registrations', confirmations: 'users/confirmations', sessions: 'users/sessions' }, :path_names => { :sign_up => "register", :sign_in => "login", :sign_out => "logout"}
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'


  get "places/:id/review" => 'places#review'
  get "places/:id/rating" => 'places#rating'
  get "account/index"
  get "users/settings"
  get "places/index"
  get "places/verify"
  get "places/number"
  get "display/index"
  get "places/done"
  #adding all resources
  resources :orders
  resources :line_items
  resources :carts
  resources :ratings
  resources :places
  resources :store
  resources :devise
  resources :users
  get 'lease/index'
  get 'booking/index'
  get 'show/:id' => 'user#show'
  match 'contact' =>"contact#new", as: 'contact', via: [:get]
  match 'contact' =>"contact#create", via: [:post]

  get "about/tos" => "about#tos", as: 'tos'
  get "about/privacy" => "about#privacy", as: 'privacy'
  get "about/team" => "about#team", as: 'team'
  get "about/hostwithus" => "about#hostwithus", as: 'hostwithus'
  get "about/howitworks" => "about#howitworks", as: 'howitworks'
  get "about/analytics" => "about#analytics", as: 'analytics'
    get "about/static" => 'about#static', as: 'static'

  get "welcome/index"
  get "users/registrations/new" =>'users#new', as: 'new' 
  get "welcome/payment" =>'welcome#payment', as: 'payment'
  get "welcome/profile" =>'welcome#profile', as: 'profile'
# You can have the root of your site routed with "root"
  root 'welcome#index'

  get "welcome/aboutus" => 'welcome#aboutus', as: 'aboutus'
  get "welcome/dashboard" => 'welcome#dashboard', as: 'dashboard'
  get "welcome/stays" => 'welcome#stays', as: 'stays'
  get "account/index" => 'account#index', as: 'index'
  get "welcome/content" => 'welcome#content', as: 'content'


   
  get "*path"=>redirect("/")
    
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
