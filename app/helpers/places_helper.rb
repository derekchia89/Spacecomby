module PlacesHelper
	def rating_ballot
    if @rating = current_user.ratings.find_by_place_id(params[:id])
        @rating
    else
        current_user.ratings.new
    end
end


def review_ballot
    if @review = current_user.reviews.find_by_place_id(params[:id])
        @review

    else
        current_user.reviews.new
    end
end

def current_user_rating
    if @rating = current_user.ratings.find_by_place_id(params[:id])
        @rating.value
    else
        "N/A"
    end
end


end
