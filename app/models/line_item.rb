class LineItem < ActiveRecord::Base
	belongs_to :place
	belongs_to :cart
	belongs_to :order

	def total_price 
		place.price * quantity
	end

	def count_days
  (place.select_end - place.select_start).to_i
 end 

 	def total_amount 
 		total_price * count_days
 	end	


 	def final_price
    line_items.to_a.sum { |item| item.total_amount }
  end
  
end
