class User < ActiveRecord::Base

  attr_accessor :current_password

  has_many :places
  has_many :ratings
  has_many :reviews
  # has_many :rated_places, :through => :ratings, :source => :places
  has_many :orders
  has_one :cart
  
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

  has_attached_file :photo, :styles => { 
    :normal => "642x335#",
    :tiny => "25x25#",
    :thumbnail => "100x100#",
    :small  => "150x150>",
    :medium => "200x200>" }, 
    :default_style => :normal

  include TwilioContactable::Contactable
  twilio_contactable

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
  user = User.where(:provider => auth.provider, :uid => auth.uid).first
  unless user
    user = User.create(name:auth.extra.raw_info.name,
                         provider:auth.provider,
                         uid:auth.uid,
                         email:auth.info.email,
                         password:Devise.friendly_token[0,20],
                         image:auth.info.image
                         )
  end
	user
  end	
  
  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
        user.image = data["image"] if user.image.blank?
      end
    end
  end

  def profileimage
    "http://graph.facebook.com/#{self.uid}/picture?height=170&width=170"
  end

  after_create :assign_default_role

  def assign_default_role
    add_role(:registered)
  end
end