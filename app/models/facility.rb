class Facility < ActiveRecord::Base
	has_many :selections
	has_many :places, :through => :selections
end
