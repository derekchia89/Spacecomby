class Place < ActiveRecord::Base
  
    geocoded_by :address
    after_validation :geocode, :if => :address_changed?
    has_many :selections
    has_many :facilities, :through => :selections
    belongs_to :user
    has_many :ratings
    has_many :order_places
    has_many :reviews

    accepts_nested_attributes_for :order_places   
   

    #:normal will create a folder name "normal" and have a version of the image with the resolution as preset below. 
    #The original file will be placed in the "original" folder
  has_attached_file :image, :styles => { 
    :normal => "642x335#",
    :tiny => "70x50#",
    :thumbnail => "100x100#",
    :small  => "150x150>",
    :medium => "300x300>" }, 
    :default_style => :normal

  searchable do 
    text :title, :description, :address, :country, :wsType
    integer :maxOccupancy
    string :wsType
  end
  
 

 include ActiveModel::Validations
 

  validates :title, :presence => true
  validates :description, :presence => true
  validates :maxOccupancy, :presence => true, :numericality => true
  validates :wsType, :presence => true
  validates :telNumber, :presence => true, :numericality =>true
  validates :startTime, :presence => true
  validates :endTime, :presence => true
  validates :price, :presence => true, :numericality =>true, :inclusion => { :in => 1..10000000 }
 
    
  

  def average_rating
    @value = 0
    self.ratings.each do |rating|
        @value = @value + rating.value
    end
    @total = self.ratings.size
    @value.to_f / @total.to_f
end



  private

  #For slider-range
  scope :filter, lambda { |low, high| { :conditions => { :price => low..high } } }

  def self.high_low_prices
    [Place.minimum(:price), Place.maximum(:price)]
  end
  #End slider-range



end
