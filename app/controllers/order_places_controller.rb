class OrderPlacesController < ApplicationController
  before_action :set_order_place, only: [:show, :edit, :update, :destroy]

  # GET /order_places
  # GET /order_places.json
  def index
    @order_places = OrderPlace.all
  end

  # GET /order_places/1
  # GET /order_places/1.json
  def show
  end

  # GET /order_places/new
  def new
    @order_place = OrderPlace.new
  end

  # GET /order_places/1/edit
  def edit
  end

  # POST /order_places
  # POST /order_places.json
  def create
    @order_place = OrderPlace.new(order_place_params)

    respond_to do |format|
      if @order_place.save
        format.html { redirect_to @order_place, notice: 'Order place was successfully created.' }
        format.json { render action: 'show', status: :created, location: @order_place }
      else
        format.html { render action: 'new' }
        format.json { render json: @order_place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_places/1
  # PATCH/PUT /order_places/1.json
  def update
    respond_to do |format|
      if @order_place.update(order_place_params)
        format.html { redirect_to @order_place, notice: 'Order place was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @order_place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_places/1
  # DELETE /order_places/1.json
  def destroy
    @order_place.destroy
    respond_to do |format|
      format.html { redirect_to order_places_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_place
      @order_place = OrderPlace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_place_params
      params.require(:order_place).permit(:order_id, :place_id)
    end
end
