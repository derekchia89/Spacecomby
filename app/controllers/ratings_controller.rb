class RatingsController < ApplicationController

	skip_before_filter :verify_authenticity_token

    def create
        @place = Place.find_by_id(params[:place_id])
        if current_user.id == @place.id
            redirect_to place_path(@place), flash[:alert] => "You cannot rate for your own place"
        else
            @rating = Rating.new(rating_params)
            @rating.place_id = @place.id
            @rating.user_id = current_user.id
            if @rating.save
                respond_to do |format|
                    format.html { redirect_to place_path(@place), :notice => "Your rating has been saved" }
                    format.js
                end 
            end
        end
    end

    def update
        @place = Place.find_by_id(params[:place_id])
        if current_user.id == @place.id
            redirect_to place_path(@place), :alert => "You cannot rate for your own place"
        else
            @rating = current_user.ratings.find_by_place_id(@place.id)
            if @rating.update_attributes(rating_params)
                respond_to do |format|
                    format.html { redirect_to place_path(@place), :notice => "Your rating has been updated" }
                    format.js
                end
            end
        end
    end

    def rating_params
        params.require(:rating).permit(:value, :place_id, :user_id)
    end
    
end