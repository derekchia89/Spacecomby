class OrdersController < ApplicationController
layout "application2"
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  # GET /orders.json
 

  # GET /orders/1
  # GET /orders/1.json

  def index
  end
  
  def show
  end

  # GET /orders/new
  
  def new 
    @cart = current_cart
  @order = Order.new(:express_token => params[:token])

end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json

  def express
  response = EXPRESS_GATEWAY.setup_purchase(current_cart.build_order.price_in_cents,
    :ip                => request.remote_ip,
    :return_url        => new_order_url,
    :cancel_return_url => places_url
  )
  redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  end

  def confirm
  @order = Order.new
  @order.express_token = params[:token]
  respond_to do |format|
    format.html # confirm.html.erb
  end
end

 
  def create
    @user = current_user

    @order = current_cart.build_order(order_params)
    @order.line_items = current_cart.line_items

   
    @order.ip_address = request.remote_ip
    @order.user_id = current_user.id
    @order.amount = current_cart.total_price

  
if @order.save

      if @order.purchase


      @order.line_items.each do |item| 
      @user = current_user
      @user = item.place.user
      @user.earning = @user.earning + item.total_amount
      @user.save

     end  

      @user = current_user

        @user.spending = @user.spending + current_cart.total_price
          @user.save

          
          @order.line_items.each do |item| 

            @order_place = OrderPlace.new
         
            @order_place.place_id = item.place.id
           
            @order_place.order_id = item.order.id

                @order_place.start_date = item.place.select_start
            @order_place.end_date = item.place.select_end
            @order_place.amount = item.total_amount
            @order_place.save
           
        
        end
      
        

        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil

        render :action => "success"

      

      else
        render :action => "failure"
      end
    else
      render :action => 'failure'
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :no_content }
    end
  end

    
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:cart_id, :ip_address, :first_name, :last_name, :card_type, :card_expires_on, :card_number, :card_verification, :express_token)
    end
 
end
