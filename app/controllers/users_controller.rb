class UsersController < ApplicationController

  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end

  

  def show

    @user = User.find(params[:id])

    
    if @user == current_user
      

      redirect_to edit_user_registration_path

    else
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user }

       end
    end

  end


 


end