 class PlacesController < ApplicationController
    layout 'application2'
    before_action :set_place, only: [:show, :edit, :update, :destroy]
    helper_method :redirect_to

  # GET /places
  # GET /places.json
  def index
    @places = Place.all
   
    @search = Place.search do
      fulltext params[:search]
    end
    @places = @search.results
    
    #For slider-range
      unless params[:low] && params[:high]
        @place = Place.all
      else
        @place = Place.filter(params[:low], params[:high])
      end  
        @price_range = Place.high_low_prices
      #End slider-range
  end

  # GET /places/1
  # GET /places/1.json
  def show
   render :layout => 'application'
   @place = Place.find(params[:id])
   #@user = User.find(:all, :conditions => {:id => current_user.id})
  end

  # GET /places/new
  def new
    if user_signed_in?
      @user = current_user
      if(@user.phone_number.blank?)
        render :action => "number"

    elsif(@user.sms_confirmed? == false)
        # fires off a TXT to the user with a generated confirmation code
        @user.send_sms_confirmation!
        render :action => "verify"
      else
        @place = Place.new
      end
    else
        redirect_to user_session_path
    end
  end


  def number
    @user = current_user
    render :action => "verify"
end

  # GET /places/1/edit
  def edit
    @user = current_user
    @place = Place.find(params[:id])
    unless @user.id == @place.user_id && user_signed_in?
    flash[:notice] = "You do not have the access to edit"
    redirect_to place_path
    end 
  end

  #For rating
  def rating
   @place = Place.find(params[:id])
  end

  def review
    @place = Place.find(params[:id])
   end 

  #For sms verification
  def verify
    @user = current_user 
    @user.sms_confirm_with(params[:verify_code])
    if(@user.sms_confirmed? == true)
      @place = Place.new
      render :action => "new"
      else
        flash.now[:error] = "Wrong code"
        render action: "verify"
      end
  end

    # POST /places
    # POST /places.json
    def create
      @place = Place.new(place_params)
      @place.save
      @user = current_user
      @place.user_id = current_user.id

      respond_to do |format|
        if @place.save
          format.html { redirect_to @place, notice: 'Place was successfully created.' }
          format.json { render action: 'show', status: :created, location: @place }
        else
          format.html { render action: 'new' }
          format.json { render json: @place.errors, status: :unprocessable_entity }
        end
      end
    end


  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    params[:place][:facility_ids] ||= []
    @place = Place.find(params[:id])
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to @place, notice: 'Place was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to :back , notice: 'Your listing has been successfully deleted.'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
        params.require(:place).permit(:title, :description, :address, :latitude, :longitude, :maxOccupancy, :wsType, :telNumber, :startTime, :endTime, :price, :rating, :review, :image, :image_file_name, :image_content_type, :image_file_size, :image_updated_at, :start_date, :end_date, :select_start, :country, :postal, :select_end, :facility_ids => [])
    end
    
  end

def static
 
end