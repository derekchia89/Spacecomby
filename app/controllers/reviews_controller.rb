class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :edit, :update, :destroy]

  # GET /reviews
  # GET /reviews.json
  

  # POST /reviews
  # POST /reviews.json
  def create
        @place = Place.find_by_id(params[:place_id])
        if current_user.id == @place.id
            redirect_to place_path(@place), :alert => "You cannot review for your own place"
        else
            @review = Review.new(review_params)
            @review.place_id = @place.id
            @review.user_id = current_user.id
            if @review.save
                respond_to do |format|
                    format.html { redirect_to place_path(@place), :notice => "Your review has been saved" }
                    format.js
                end 
            end
        end
    end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
        @place = Place.find_by_id(params[:place_id])
        if current_user.id == @place.id
            redirect_to place_path(@place), :alert => "You cannot review for your own place"
        else
            @review = current_user.reviews.find_by_place_id(@place.id)
            if @review.update_attributes(review_params)
                respond_to do |format|
                    format.html { redirect_to place_path(@place), :notice => "Your review has been updated" }
                    format.js
                end
            end
        end
    end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review.destroy
    respond_to do |format|
      format.html { redirect_to reviews_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:place_id, :user_id, :content)
    end
end
