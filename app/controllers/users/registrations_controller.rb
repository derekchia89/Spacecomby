class Users::RegistrationsController < Devise::RegistrationsController

  # def create
  #   if session[:omniauth] == nil #OmniAuth
  #       super
  #       session[:omniauth] = nil unless @user.new_record? #OmniAuth
  #     else
  #       build_resource
  #       clean_up_passwords(resource)
  #       flash[:alert] = "There was an error with the recaptcha code below. Please re-enter the code."
  #       #use render :new for 2.x version of devise
  #       render :new 
  #     end
  #   else
  #     super
  #     session[:omniauth] = nil unless @user.new_record? #OmniAuth
  #   end
  # end

  def edit
     render layout: 'dashboard'
  end

    def update

    # required for settings form to submit when password is left blank
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(user_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to :back
    else
      flash[:alert] = "There was an error with the password entered."
      redirect_to :back
      
    end

  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :gender, :email, :current_password, :password, :password_confirmation, :phone_number, :description, :address, :country, :name, :photo, :image, :image_file_name, :image_content_type, :image_file_size, :image_updated_at)
  end
end
