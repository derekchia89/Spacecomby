  class ApplicationController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :null_session

    helper_method :redirect_to


  #customize default devise registration fields
  before_filter :configure_devise_params, if: :devise_controller?

  def configure_devise_params
   devise_parameter_sanitizer.for(:sign_up) do |u|
     u.permit(:first_name, :last_name, :gender, :email, :password, :password_confirmation, :phone_number, :address, :country)
   end
   devise_parameter_sanitizer.for(:account_update) do |u|
    u.permit(:first_name, :last_name, :gender, :email, :current_password, :password, :password_confirmation, :phone_number, :address, :country, :photo)
    end
  end

  after_filter :store_location

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    if (request.fullpath != "/users/login" &&
      request.fullpath != "/users/logout" &&
      request.fullpath != "/users/password" &&
    !request.xhr?) # don't store ajax calls
    session[:previous_url] = request.fullpath
    end
  end

  def after_sign_in_path_for(resource)
    session[:previous_url] || welcome_index_path
  end

  def after_sign_up_path_for(resource)
     new_session_path
  end 




  private

  def current_place
   Place.find(session[:place_id])
  end


  def current_cart 
    Cart.find(session[:cart_id])
  rescue ActiveRecord::RecordNotFound
    cart = Cart.create
    session[:cart_id] = cart.id
    cart
  end
  helper_method :current_cart

   protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) do |u|
       u.permit(:first_name, :last_name, :gender, :email, :password, :password_confirmation, :phone_number, :address, :country)
      end
      devise_parameter_sanitizer.for(:account_update) do |u|
        u.permit(:first_name, :last_name, :gender, :email, :current_password, :password, :password_confirmation, :phone_number, :address, :country)
      end
    end
    
  end