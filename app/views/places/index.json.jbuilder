json.array!(@places) do |place|
  json.extract! place, :title, :description, :maxOccupancy, :wsType, :telNumber, :startTime, :endTime, :price, :rating
  json.url place_url(place, format: :json)
end
