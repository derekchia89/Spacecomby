json.array!(@order_places) do |order_place|
  json.extract! order_place, :order_id, :place_id
  json.url order_place_url(order_place, format: :json)
end
