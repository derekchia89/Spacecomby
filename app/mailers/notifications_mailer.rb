class NotificationsMailer < ActionMailer::Base

  default :from => "info@spacecomby.com"
  default :to => "info@spacecomby.com"

  def new_message(message)
    @message = message
    mail(:subject => "[Spacecomby] #{message.subject}")
  end

end