#!/bin/bash
echo "Intializing packages"

bundle install

clear

echo "Intializing database"

rake db:drop RAILS_ENV="production"
rake db:create RAILS_ENV="production"
rake db:migrate RAILS_ENV="production"

clear

echo "Seeding database"

rake db:seed RAILS_ENV="production"

clear

echo "Welcome to SpaceComby!"

rails s -e production
