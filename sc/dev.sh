#!/bin/bash

echo "Intializing packages"

bundle install

clear

echo "Intializing database"

rake db:drop
rake db:create
rake db:migrate

clear

echo "Seeding database"

rake db:seed 

clear

echo "Welcome to SpaceComby!"

rails s
