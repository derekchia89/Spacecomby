http://spacecomby.com

If <bundle install> encountered the following error:

nokogiri gem Gem::Installer::ExtensionBuildError: 
ERROR: Failed to build gem native extension. /usr/local/rvm/rubies/ruby-2.0.0-p247/bin/ruby extconf.rb

Do this for Mac:

$ brew install libxslt libxml2 libiconv

$ NOKOGIRI_USE_SYSTEM_LIBRARIES=1 bundle install
